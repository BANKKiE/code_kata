const countPositivesSumNegatives = (list) => {
  var num = list.length;
  var sum = [0,0];
  var a = 0,b = 0;
  for(var i=0;i<num;i++){
    if(list[i] > 0)
      a++;
    else if(list[i]<0)
      b = b + list[i];
  }
  return [a,b];
}

module.exports = countPositivesSumNegatives
