const reverseOrder = (list) => {
  var num = list.length;
  var reverseOrderList = new Array();
  var i = 0;
  var j = num-1;
  for(i=0;i<num;i++){
    reverseOrderList[i] = list[j];
    j--;
  }
  return reverseOrderList;
}
module.exports = reverseOrder
